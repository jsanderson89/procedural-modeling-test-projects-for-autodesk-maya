import maya.cmds as mc;

def spikes(subdivVertical=25, subdivOther=2):
	cube = createCube(subdivVertical ,subdivOther);
	listOfFaces = compileListOfExtrudableFaces(cube, subdivVertical, subdivOther);
	print listOfFaces;
	extrudeAllFaces(listOfFaces);

def createCube(subdivY,subdiv):
	cube = mc.polyCube(w=5, h=30, d=5, sx=subdiv, sy=subdivY, sz=subdiv);
	return cube;

def compileListOfExtrudableFaces(obj, vertSub, otherSub):
	returnListOfFaces = [];
	mc.select(obj[0]);
	numberOfFaces = mc.polyEvaluate(face=1);
	
	for i in range(0,numberOfFaces,1):
		returnListOfFaces.append(obj[0] + ".f" + "[" + str(i) + "]");
	
	returnListOfFaces = removeBottomCap(returnListOfFaces, vertSub, otherSub);
	returnListOfFaces = removeTopCap(returnListOfFaces, vertSub, otherSub);
	
	# for i in range(0, len(returnListOfFaces), 1):
		# mc.select(returnListOfFaces[i], add=1);
	print returnListOfFaces;
	return returnListOfFaces;

#end of first face = 49 = (25*2)-1
#topCap = [sy*2], lastFace = sy*2 + ((sz*2)-1)
#bottomCap = [((sy*sz)*2)+(sz*2)], lastFace = [faceOne + ((sz*2)-1)]	

def removeBottomCap (list, vertSub, otherSub):
	numberOfFaces = (otherSub*otherSub);
	firstFace = ((vertSub*otherSub)*2) + (otherSub *2);
	
	for i in range(numberOfFaces,0, -1):
		faceToDelete = (firstFace+i)-1;
		print faceToDelete;
		del list[faceToDelete];
	return list;
	
def removeTopCap (list, vertSub, otherSub):
	#print "called";
	numberOfFaces = (otherSub*otherSub);
	firstFace = (vertSub*otherSub);
	for i in range(numberOfFaces,0, -1):
		faceToDelete = (firstFace+i)-1;
		print faceToDelete;
		del list[faceToDelete];
	return list;

def extrudeAllFaces(faceList):
	#for i in range(0,len(faceList),1):
	for i in range(len(faceList)-1,-1,-1):
		extrudeSpike(faceList[i]);

def extrudeSpike(face):
	extruVal = 6;
	print "-----------------------------------------";
	print "EXTRUDESPIKE";
	print "face";
	print face;
	#print "extrudeSpike";
	faceNormal = [];
	faceNormal = returnFaceNormalDirections(face);
	#print faceNormal;
	mc.select(face);
	
	xv = faceNormal[0];
	print "xv";
	print xv;
	yv = faceNormal[1];
	print "yv";
	print yv;
	zv = faceNormal[2];
	print "zv";
	print zv;
	
	if (xv != 0):
		if (xv < 0):
			xv = -extruVal;
		if (xv > 0):
			xv = extruVal;
		
	if (yv != 0):
		if (yv < 0):
			yv = -extruVal;
		if (yv > 0):
			yv = extruVal;
		
	if (zv != 0):
		if (zv < 0):
			zv = -extruVal;
		if (zv > 0):
			zv = extruVal;
	
	extrusion = mc.polyExtrudeFacet(face);
	print "extruding face";

	print extrusion;
	mc.select(face);
	mc.move(zv,yv,xv,r=1);
	mc.polyCollapseFacet(face);
	print "-----------------------------------------";
	
def returnFaceNormalDirections(face):
	mc.select(face);
	faceNormalInfo = mc.polyInfo(fn=1);
	faceNormalParsed = faceNormalInfo[0].split(" ");
	#'FACE_NORMAL    129: 1.000000 0.000000 0.000000\n'
	#will return at [6], [7] and [8]
	faceNormalParsed[len(faceNormalParsed)-1] = faceNormalParsed[len(faceNormalParsed)-1].replace("\n", "");
	#print "faceNormalParsed:"
	#print faceNormalParsed;
	
	faceNormal = [];
	faceNormal.append(faceNormalParsed[len(faceNormalParsed)-1]);
	faceNormal.append(faceNormalParsed[len(faceNormalParsed)-2]);
	faceNormal.append(faceNormalParsed[len(faceNormalParsed)-3]);
	
	for i in range(0,len(faceNormal),1):
		faceNormal[i] = float(faceNormal[i]);
	
	#print "faceNormal:";
	#print faceNormal;
	
	return faceNormal;

spikes(35,5);





