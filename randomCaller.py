###################################
###################################
#proceduralTest

import maya.cmds as mc;
import math as math;
from random import randint;
from random import uniform;


def Main():
	constructInitialGeometry()
	
def constructInitialGeometry():
	mc.polyPlane();
	
	
Main()###################################
###################################
#genBubbles

import maya.cmds as mc;
from random import randint;
from random import choice;
from random import uniform;

def createBubbles(maxNumberOfBubbles=50):
	numberOfSpheres = randint(0,maxNumberOfBubbles);
	listOfObjects = createBase();
	
	for i in range(0, numberOfSpheres+1, 1):
		print "#########################################"
		print i;
		randomObject = choice(listOfObjects);
		# print randomObject[0];
		
		listOfFaces = getObjectFaces(randomObject);
		baseFace = choice(listOfFaces);
		
		makeSphere = createSphere(baseFace, randomObject[0]);
		# print makeSphere;
		
		listOfObjects.append(makeSphere);
		print "sphere {} of {} created.".format(i,numberOfSpheres);
		mc.refresh();
		print "#########################################"
	

def createBase():
	returnList = [];
	baseSphere = mc.polySphere(r=2, sx=20,sy=20,ax=(0,1,0))
	returnList.append(baseSphere);
	return returnList;

def selRandomObject(listOfObjects):
	selection = choice(listOfObjects);
	return selection;
	
def getObjectFaces(obj):
	returnListOfFaces = [];
	numberOfFaces = mc.polyEvaluate(obj[0], face=1);
	
	for i in range(0,numberOfFaces,1):
		returnListOfFaces.append(obj[0] + ".f" + "[" + str(i) + "]");
	
	print "return LOF"
	print returnListOfFaces;
	return returnListOfFaces;
	
def createSphere(baseFace, object):
	randomRadius = uniform(0,2);
	print randomRadius;
	
	faceVertsQuery = mc.polyInfo(baseFace, faceToVertex=1);
	faceVerts = getVertsFromQuery(faceVertsQuery, object);
	print faceVerts;
	
	vertCoords = getVertCoords(faceVerts);
	print vertCoords;
	
	centerPoint = getFaceCenter(vertCoords);
	print "cp"
	print centerPoint[0];
	
	createdSphere = mc.polySphere(r=randomRadius);
	mc.xform(createdSphere[0], translation = (centerPoint[0],centerPoint[1],centerPoint[2]), a=1);
	
	return createdSphere;

def getVertsFromQuery(query, obj):
	infoList = query[0].split("    ");
	print infoList;
	infoList[len(infoList)-1] = infoList[len(infoList)-1].replace(" \n", "");
	
	del infoList[1];
	del infoList[0];
	
	for i in range(0,len(infoList), 1):
		infoList[i].strip();
		infoList[i] = (obj + ".vtx" + "[" + infoList[i] + "]");
	
	return infoList;

def getVertCoords(vertList):
	outputList = [];
	for i in range(0,len(vertList),1):
		outputList.append(mc.xform(vertList[i], query = 1, worldSpace=1, translation=1));
		print outputList[i];
	
	return outputList;
	
def getFaceCenter(polygonVertList):
	px=0;
	py=0;
	pz=0;
	
	for i in range (0,len(polygonVertList),1):
		px += polygonVertList[i][0];
		py += polygonVertList[i][1];
		pz += polygonVertList[i][2];
	
	cp = [px/len(polygonVertList), py/len(polygonVertList),pz/len(polygonVertList)];
	
	print "creating locator....";
	mc.spaceLocator(p=(cp[0], cp[1],cp[2]));
	
	return cp;



###################################
###################################
#turtlegraphics_3D

import maya.cmds as mc;
import maya.mel as mel;
import math as math;
from random import randint;
from random import uniform;


class drawer(object):
	x = 0;
	y = 0;
	z = 0;
	
	prevX = 0;
	prevY = 0;
	prevZ = 0;
	
	#XZ orientation
	orientationY = 0;
	prevOrientationY = 0;
	#XY orientation
	orientationZ = 0;
	prevOrientationZ = 0;
	radius = 0;

	def __init__(self):
		self.x = 0;
		self.y = 0;
		self.orientationY = 0;
		self.prevOrientationY = 0;
		self.orientationZ = 0;
		self.prevOrientationZ = 0;
		self.radius = 1;
		
	def resetO(self):
	    self.prevOrientationY = self.orientationY;
	    self.prevOrientationZ = self.orientationZ;
	    self.orientationY = 0;
	    self.orientationZ = 0;
	
	def resetOY(self):
	    self.prevOrientationY = self.orientationY;
	    self.orientationY = 0;
		
	def resetOZ(self):
	    self.prevOrientationZ = self.orientationZ;
	    self.orientationZ = 0;
	
	def resetP(self):
		self.setX(0);
		self.setY(0);
		self.setZ(0);
		
	def setX(self,num):
		self.prevX = self.x;
		self.x = num;
		
	def setY(self,num):
		self.prevY = self.y;
		self.y = num;
		
	def setZ(self,num):
		self.prevZ = self.z;
		self.z = num;
	
	def draw (self, distance):
		d = 1;
		
		#newX = (distance * (math.cos(self.orientation))) + self.x;
		#newY = self.y;
		#newZ = (distance * (math.sin(-self.orientation))) + self.z;
		
		self.setX(self.x + ((distance * math.cos(self.orientationZ)) * (math.sin(self.orientationY))));
		self.setY(self.y + (distance * math.sin(self.orientationZ)));
		self.setZ(self.z + ((distance * math.cos(self.orientationZ)) * (math.cos(self.orientationY))));
		
		drawCurve = mc.curve(d= 1, p=[(self.prevX,self.prevY,self.prevZ), (self.x,self.y,self.z)]);
		print "drawCurve:";
		print drawCurve;
		
		drawCylinder = self.createCylinder(self.prevX, self.prevY, self.prevZ, self.x, self.y, self.z);
		
		mc.select(drawCylinder[0]);
		
		mc.polyExtrudeFacet(inputCurve=drawCurve);
		mc.refresh();
		
	def turn(self, degreeY, degreeZ=0):
		print ">>>>>>>>>>>>>>>>>>>>>>>>"
		print "TURN"
		self.prevOrientationY = self.orientationY;
		self.prevOrientationZ = self.orientationZ;
		self.orientationY += math.radians(degreeY);
		self.orientationZ += math.radians(degreeZ);
			
		print "orientation:";
		print self.orientationY;
		print self.orientationZ;
		print ">>>>>>>>>>>>>>>>>>>>>>>>"
		
		
	#internal functions = do not call	
	def createCylinder(self, x, y, z, endX, endY, endZ):
		subdivs = 20;

		thisCylinder = mc.polyCylinder(radius=self.radius);

		mc.setAttr(thisCylinder[1]+".subdivisionsCaps", 1);

		mc.select(thisCylinder[0] + ".f" + "[" + str(0) + ":" + str((subdivs*2)-1) + "]");
		mc.delete();
		
		loc = mc.spaceLocator(p=(endX, endY, endZ));
		mc.xform(loc, cp=True);
		
		mc.xform(thisCylinder[0], cp=True);
		mc.select(thisCylinder[0]);
		mc.move(x,y,z, rpr=True);
		
		mc.aimConstraint(loc, thisCylinder[0], aimVector=(0,1,0));

		return thisCylinder;
		
	def returnReferenceAngle(self, angleAsDegrees):
	    returnAngle = 0;
	    if(angleAsDegrees >= 0 and angleAsDegrees <=90):
	        returnAngle = angleAsDegrees;
	    if(angleAsDegrees > 90 and angleAsDegrees <= 180):
	        returnAngle = 180 - angleAsDegrees;
	    if(angleAsDegrees > 180 and angleAsDegrees <= 270):
	        returnAngle = angleAsDegrees - 180;
	    if(angleAsDegrees > 270 and angleAsDegrees <= 360):
	        returnAngle = 360 - angleAsDegrees;
	    return returnAngle;

	def getVector(self, xOne,yOne,zOne, xTwo,yTwo,zTwo):
		xRes = xTwo-xOne;
		yRes = yTwo-yOne;
		zRes = zTwo-zOne;
		return xRes,yRes,zRes;
		

		
		
		
		
		
		
		
		
		
		
		
		
def drawOne():
	d = drawer();
	d.draw(5);
	
def drawFlatSquare():
	d = drawer();
	d.draw(5);
	d.turn(90,0);
	d.draw(5);
	d.turn(90,0);
	d.draw(5);
	d.turn(90,0);
	d.draw(5);
	
def drawVertSquare():
	d = drawer();
	d.draw(5);
	d.turn(0, 90);
	d.draw(5);
	d.turn(0, 90);
	d.draw(5);
	d.turn(0, 90);
	d.draw(5);
	
def drawAngledSquare():
	d = drawer();
	d.turn(45,45);
	d.draw(5);
	d.turn(0, 90);
	d.draw(5);
	d.turn(0, 90);
	d.draw(5);
	d.turn(0, 90);
	d.draw(5);
	
def drawStar():
	d=drawer();
	for j in range(0,12,1):
	    d.turn(15,15);
	    for i in range(0,10,1):
			d.draw(10);
			d.turn(0,90);
			d.draw(10);
			d.turn(0,90);
			d.draw(10);
			d.turn(0,90);
			d.draw(10);
			d.turn(0,10);
			
def drawRando():
	d=drawer();
	randDeg = randint(1,90);
	randNumStars = randint(1,50);
	randTurn = randint(1,15);
	randNumPoints = randint(1,50);
	randDrawLength = randint(1,10);
	randSTurnY = randint(0,5);
	randSTurnZ = randint(10,180);
	
	for i in range(0,randint(1,50),1):
		print "i"
		print "************************************************************************************************"
		d.resetO();
		d.turn(randDeg*i, randDeg*i);
		
		for j in range(0,randNumStars,1):
			print "j"
			print "************************************************************************************************"
			d.turn(0,randTurn);
			for k in range (0,randNumPoints,1):
				print "k"
				print "************************************************************************************************"
				d.draw(randDrawLength);
				d.turn(randSTurnY,randSTurnZ);

				
def drawRandoTwo():
	d=drawer();
	#d.radius = 1;
	#d.radius = uniform(0,1);
	d.radius = 1;
	randDeg = randint(1,90);
	randDegCurrent = randDeg;
	randNumStars = randint(1,30);
	randTurn = randint(1,15);
	randNumPoints = randint(1,10);
	#at 5,15 and radius 5, tends to produce "rose" structures
	randDrawLength = randint(15,35);
	randSTurnY = randint(0,5);
	randSTurnZ = randint(10,180);
	randSTurnYTwo = randint(0,5);
	randSTurnZTwo = randint(10,180);
	
	for i in range(0,randint(1,10),1):
		print "i"
		print "************************************************************************************************"
		d.resetO();
		d.resetP();
		d.turn(randDegCurrent, randDegCurrent);
		randDegCurrent += randDeg;
		
		for j in range(0,randNumStars,1):
			print "j"
			print "************************************************************************************************"
			d.turn(0,randTurn);
			for k in range (0,randNumPoints,1):
				print "k"
				print "************************************************************************************************"
				d.draw(randDrawLength);
				d.turn(randSTurnY,randSTurnZ);
				d.draw(randDrawLength);
				d.turn(randSTurnYTwo,randSTurnZTwo);
			
def drawRandoThree():
	d=drawer();
	randDeg = randint(1,90);
	randNumStars = randint(1,50);
	randTurn = randint(1,15);
	randNumPoints = randint(1,50);
	randDrawLength = randint(1,10);
	randSTurnY = randint(0,5);
	randSTurnZ = randint(10,180);
	
	for i in range(0,randint(1,50),1):
		print "i"
		print "************************************************************************************************"
		d.resetO();
		d.turn(randDeg*i, randDeg*i);

		for k in range (0,randNumPoints,1):
			print "k"
			print "************************************************************************************************"
			d.draw(randDrawLength);
			d.turn(randSTurnY,randSTurnZ);

def drawRandoFour():
	d=drawer();
	#d.radius = 1;
	#d.radius = uniform(0,1);
	d.radius = 5;
	randDeg = randint(1,90);
	randDegCurrent = randDeg;
	randNumStars = randint(1,30);
	randTurn = randint(1,15);
	randNumPoints = randint(5,15);
	#at 5,15 and radius 5, tends to produce "rose" structures
	randDrawLength = randint(5,15);
	randSTurnY = randint(0,5);
	randSTurnZ = randint(10,180);
	randSTurnYTwo = randint(0,5);
	randSTurnZTwo = randint(10,180);
	
	for i in range(0,randint(1,10),1):
		print "i"
		print "************************************************************************************************"
		d.resetO();
		d.resetP();
		d.turn(randDegCurrent, randDegCurrent);
		randDegCurrent += randDeg;
		
		for j in range(0,randNumStars,1):
			print "j"
			print "************************************************************************************************"
			d.turn(0,randTurn);
			for k in range (0,randNumPoints,1):
				print "k"
				print "************************************************************************************************"
				d.draw(randDrawLength);
				d.turn(randSTurnY,randSTurnZ);
				d.draw(randDrawLength);
				d.turn(randSTurnYTwo,randSTurnZTwo);
				
def drawRandoFive():
	d=drawer();
	#d.radius = 1;
	#d.radius = uniform(0,1);
	d.radius = randint(1,5);
	randDeg = randint(1,90);
	randDegCurrent = randDeg;
	randNumStars = randint(1,5);
	randTurn = randint(1,15);
	randNumPoints = randint(1,15);
	#at 5,15 and radius 5, tends to produce "rose" structures
	randDrawLength = randint(5,15);
	randSTurnY = randint(0,180);
	randSTurnZ = randint(0,180);
	randSTurnYTwo = randint(0,180);
	randSTurnZTwo = randint(0,180);
	
	for i in range(0,randint(1,5),1):
		print "i"
		print "************************************************************************************************"
		d.resetO();
		d.resetP();
		d.turn(randDegCurrent, randDegCurrent);
		randDegCurrent += randDeg;
		
		for j in range(0,randNumStars,1):
			print "j"
			print "************************************************************************************************"
			d.turn(0,randTurn);
			for k in range (0,randNumPoints,1):
				print "k"
				print "************************************************************************************************"
				d.draw(randDrawLength);
				d.turn(randSTurnY,randSTurnZ);
				d.draw(randDrawLength);
				d.turn(randSTurnYTwo,randSTurnZTwo);
				








###################################
###################################
#caller




def doRandom():
    print "ok";
    idNumber = randint(1,10);
    #idNumber = 5;
    
    if idNumber == 1:
        print "************************************************************************************************"
        print "************************************************************************************************"
        print "1"
        print "************************************************************************************************"
        print "************************************************************************************************"
        drawRando();
        finalize();
        
    elif idNumber == 2:
        print "************************************************************************************************"
        print "************************************************************************************************"
        print "2"
        print "************************************************************************************************"
        print "************************************************************************************************"
        drawRandoTwo();
        finalize();

    elif idNumber == 3:
        print "************************************************************************************************"
        print "************************************************************************************************"
        print "3"
        print "************************************************************************************************"
        print "************************************************************************************************"
        drawRandoThree();
        finalize();

    elif idNumber == 4:
        print "************************************************************************************************"
        print "************************************************************************************************"
        print "4"
        print "************************************************************************************************"
        print "************************************************************************************************"
        drawRandoFour();
        finalize();

    elif idNumber == 5:
        print "************************************************************************************************"
        print "************************************************************************************************"
        print "5"
        print "************************************************************************************************"
        print "************************************************************************************************"
        drawRandoFive();
        finalize();

    elif idNumber >= 6:
        print "************************************************************************************************"
        print "************************************************************************************************"
        print idNumber;
        print "************************************************************************************************"
        print "************************************************************************************************"
        createBubbles(randint(15,200));
        finalize();

    else:
        print "************************************************************************************************"
        print "************************************************************************************************"
        print "NOT FOUND"
        print "************************************************************************************************"
        print "************************************************************************************************"



def finalize():
    mel.eval("incrementAndSaveScene 0");
    
    mc.select(all=True);
    sceneName = mc.file(query=True, sceneName=True);
    sceneName = sceneName[:-3];
    sceneName = sceneName + ".stl"
    print sceneName;
    
    mc.file(sceneName, exportSelected=True, force=True, type="STL_DCE");
    mc.delete();   
    

def randomCaller(numberOfIncrements):
    for i in range(0,numberOfIncrements,1):
        doRandom();
        

#drawOne();
#drawFlatSquare();
#drawVertSquare();
#drawAngledSquare();
#drawStar();
#drawRando();
#drawRandoTwo();
#drawRandoThree();
#drawRandoFour();
#drawRandoFive();'
randomCaller(5);