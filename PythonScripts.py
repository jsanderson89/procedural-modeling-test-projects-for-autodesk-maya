import maya.cmds as mc;
from random import choice;

def extrudeFromRandomFace( list ):
        faceToExtrude = choice(list);
        print faceToExtrude;
        mc.polyExtrudeFacet(faceToExtrude, ltz=2);

def createCube():
    numberOfSubdivs = 4;
    modelToModify = mc.polyCube(w=8, h=2, d=8, sx=numberOfSubdivs, sy=1, sz=numberOfSubdivs, ax=(0,1,0));

    #start face is numberOfSubdivs
    #last face is numSubdivs * numSubdivs + (numSubdivs-1)
    lastFace = (numberOfSubdivs * numberOfSubdivs) + (numberOfSubdivs - 1);
    print ("last face should be " + str(lastFace));
    
    selectedFaces = [];
    
    for i in range(numberOfSubdivs, lastFace+1, 1):
        selectedFaces.append(modelToModify[0] + "." + "f[%d]" %i);
        
    print (selectedFaces);
    for i in range(0,40,1):
        extrudeFromRandomFace(selectedFaces);


createCube();


















# current Issues:
	# !Produces non-manifold geometry
			# Fix: Implement check each draw call to check if faces are intersecting
				# should also prevent flat faces (perhaps a seperate fix)
					# Flat faces occur when two conflicting dimensions are chosen.
	# !produces uniform angles
			# Fix: randomize the move digits in the randomArray between positive and negative
				# should also allow cubes to "wind"
	#! reselect faces after each extrusion action



import maya.cmds as mc;
from random import choice;

def randomlyDistributeDigitsToArray():
    digit = [0, 2, 2];
    distributedArray = [];
    distributedArray.append(choice(digit));
    digit.remove(distributedArray[0]);
    distributedArray.append(choice(digit));
    digit.remove(distributedArray[1]);
    distributedArray.append(digit[0]);
    print(distributedArray);
    return distributedArray[0], distributedArray[1], distributedArray[2];

def extrudeFromRandomFace( face ):
    print(face);
    myArray = randomlyDistributeDigitsToArray();
    mc.polyExtrudeFacet(face, ltx=myArray[0], lty=myArray[1], ltz=myArray[2]);

def createCube():
    numberOfSubdivs = 4;
    modelToModify = mc.polyCube(w=8, h=8, d=8, sx=numberOfSubdivs, sy=numberOfSubdivs, sz=numberOfSubdivs, ax=(0,1,0));
    listOfFaces = [];
    num = mc.polyEvaluate(modelToModify[0], face=1);
    print num;

    for i in range(0, num, 1):
        print("i is " + str(i));
        listOfFaces.append(modelToModify[0] + "." + "f[%d]" %i);
        print(listOfFaces);

    for j in range(0, 900, 1):
        print("Something is wrong");
        faceToExtrude = choice(listOfFaces);
        extrudeFromRandomFace(faceToExtrude);



createCube();








# Next iteration:
	# apply to CVs/curve, then loft it.	
	# experiment with application of deformers to generated geometry.
	# extrude and randomize individual polyEdges/tris to produce cloudlike forms
	
	
	
	
	

	



######FOUND CODE
##intersection of line and plane

# generic math functions
def add_v3v3(a, b):
    return [a[0] + b[0],
            a[1] + b[1],
            a[2] + b[2]]


def sub_v3v3(a, b):
    return [a[0] - b[0],
            a[1] - b[1],
            a[2] - b[2]]


def dot_v3v3(a, b):
    return (a[0] * b[0] +
            a[1] * b[1] +
            a[2] * b[2])


def mul_v3_fl(a, f):
    a[0] *= f
    a[1] *= f
    a[2] *= f


# intersection function
def isect_line_plane_v3(p0, p1, p_co, p_no, epsilon=0.000001):

    # p0, p1: define the line
    # p_co, p_no: define the plane:
        # p_co is a point on the plane (plane coordinate).
        # p_no is a normal vector defining the plane direction; does not need to be normalized.

    # return a Vector or None (when the intersection can't be found).

    u = sub_v3v3(p1, p0)
    w = sub_v3v3(p0, p_co)
    dot = dot_v3v3(p_no, u)

    if abs(dot) > epsilon:
        # the factor of the point between p0 -> p1 (0 - 1)
        # if 'fac' is between (0 - 1) the point intersects with the segment.
        # otherwise:
        #  < 0.0: behind p0.
        #  > 1.0: infront of p1.
        fac = -dot_v3v3(p_no, w) / dot
        mul_v3_fl(u, fac)
        return add_v3v3(p0, u)
    else:
        # The segment is parallel to plane
        return None

##converted from blender; found http://stackoverflow.com/questions/5666222/3d-line-plane-intersection











###################################
###################################

# OLD VERSION = PRE OM


check for intersections
# import maya.cmds as mc;
# import maya.api.OpenMaya as om;


# class point(object):
	# def __init__(self):
		# self.x = 0;
		# self.y = 0;
		# self.z = 0;

# class intersection(object):
	# def __init__(self):
		# self.x = 0;
		# self.y = 0;
		# self.z = 0;
		# self.edge = "";
		# self.face = "";

# def checkForIntersections(objectOne, objectTwo):
	# objectOneFaces = getFaces(objectOne);
	# objectTwoFaces = getFaces(objectTwo);
	# faceIntersects = getFaceIntersects(objectOneFaces[0], objectTwoFaces[0]);

# def getFaces(obj):
	# numberOfFaces = mc.polyEvaluate(obj, face=1);
	# objFaces = [];
	# for i in range(0,numberOfFaces,1):
		# objFaces.append(obj + "." + "f[%d]" %i);
	# return objFaces;

# def getFaceIntersects(objectOneFace, objectTwoFace):
	# print "edges One";
	# faceEdgesOne = returnFaceEdges(objectOneFace);
	# print faceEdgesOne;
	# print "edges Two";
	# faceEdgesTwo = returnFaceEdges(objectTwoFace);
	# print faceEdgesTwo;
	
	# intersectionPoints = [];
	
	# for i in range(0,len(faceEdgesOne),1):
		# for j in range (0,len(faceEdgesTwo),1):
			# edgeVerts = returnEdgeVerts(faceEdgesOne[0]);
			# print edgeVerts;
	
	# return False;

# def returnFaceEdges(face):
	# faceNameSplit = face.split(".");
	# componentName = faceNameSplit[0];
	
	# print "face is ";
	# print face;
	# faceEdges = mc.polyInfo(face, faceToEdge=1);
	# print "face edges";
	# print faceEdges;
	# listFaceEdges = faceEdges[0].split("      ")
	# listFaceEdges[(len(listFaceEdges)-1)] = listFaceEdges[(len(listFaceEdges)-1)].replace(" \n","");
	
	# del listFaceEdges[1];
	# del listFaceEdges[0];
	
	# faceEdgesValues = [];
	
	# for i in range(0,len(listFaceEdges),1):
		# value = (componentName + "." + "e[%d]" %int(listFaceEdges[i]));
		# print value;
		# faceEdgesValues.append(value);
	
	# return faceEdgesValues;

# def returnEdgeVerts(edge):
	# edgeNameSplit = edge.split(".");
	# componentName = edgeNameSplit[0];
	
	# mc.select(edge);
	# edgePoints = mc.polyInfo(edgeToVertex=1);
	# listEdgePoints = edgePoints[0].split("      ")
	# listEdgePoints[(len(listEdgePoints)-1)] = listEdgePoints[(len(listEdgePoints)-1)].replace("  Hard\n","");
	
	# del listEdgePoints[1];
	# del listEdgePoints[0];
	
	# edgePointValues = [];
	
	# for i in range(0,len(listEdgePoints),1):
		# edgePointValues.append(componentName + "." + "vtx[%d]" %int(listEdgePoints[i]));
	
	# return edgePointValues;

# def checkForIntersects(edgeVertList, faceVertList):
	
	# print "empty";

# def addPoint(pointA,pointB):
	# returnPoint = point();
	# returnPoint.x = (pointA.x + pointB.x);
	# returnPoint.y = (pointA.y + pointB.y);
	# returnPoint.z = (pointA.z + pointB.z);
	# return returnPoint;

# def subtractPoint(pointA, pointB):
	# returnPoint = point();
	# returnPoint.x = (pointA.x - pointB.x);
	# returnPoint.y = (pointA.y - pointB.y);
	# returnPoint.z = (pointA.z - pointB.z);
	# return returnPoint;
	
# def dotProduct(pointA, pointB):
	# return [(pointA.x * pointB.x) + (pointA.y * pointB.y) + (pointA.z * pointB.z)];

# def multiplyByFactor(pointA, factor):
	# pointA.x *= factor;
	# pointA.y *= factor;
	# pointA.z *= factor;

# def returnNormalOfFace(polyInfoString):
	# returnPoint = point();
	# str

#make sure it's the actual object node - ie, the [0] val
checkForIntersections("pCube1", "pCube2")









	











###################################
###################################
#generateCurve


import maya.cmds as mc;

pointOne = (0,0,0);
pointTwo = (-4.086484,0,-0.963398);
pointThree = (-5,0,-7);
pointFour = (-10,-3,-15);
curvePoints = [pointOne, pointTwo, pointThree, pointFour];

mc.curve(p=curvePoints);

#mc.curve( p=[(0, 0, 0), (3, 5, 6), (5, 6, 7), (9, 9, 9), (12, 10, 2)], k=[0,0,0,1,2,2,2] )