#check for intersections
import maya.cmds as mc;
import maya.api.OpenMaya as om;


class intersection(object):
	def __init__(self):
		self.x = 0;
		self.y = 0;
		self.z = 0;
		self.edge = "";
		self.face = "";

def checkForIntersections(objectOne, objectTwo):
	objectOneFaces = getFaces(objectOne);
	objectTwoFaces = getFaces(objectTwo);
	faceIntersects = getFaceIntersects(objectOneFaces[0], objectTwoFaces[0]);

def getFaces(obj):
	numberOfFaces = mc.polyEvaluate(obj, face=1);
	objFaces = [];
	for i in range(0,numberOfFaces,1):
		objFaces.append(obj + "." + "f[%d]" %i);
	return objFaces;

def getFaceIntersects(objectOneFace, objectTwoFace):
	print "edges One";
	faceEdgesOne = returnFaceEdges(objectOneFace);
	print faceEdgesOne;
	print "edges Two";
	faceEdgesTwo = returnFaceEdges(objectTwoFace);
	print faceEdgesTwo;
	
	intersectionPoints = [];
	
	for i in range(0,len(faceEdgesOne),1):
		for j in range (0,len(faceEdgesTwo),1):
			edgeVerts = returnEdgeVerts(faceEdgesOne[0]);
			print edgeVerts;
	
	return False;

def returnFaceEdges(face):
	faceNameSplit = face.split(".");
	componentName = faceNameSplit[0];
	
	print "face is ";
	print face;
	faceEdges = mc.polyInfo(face, faceToEdge=1);
	print "face edges";
	print faceEdges;
	listFaceEdges = faceEdges[0].split("      ")
	listFaceEdges[(len(listFaceEdges)-1)] = listFaceEdges[(len(listFaceEdges)-1)].replace(" \n","");
	
	del listFaceEdges[1];
	del listFaceEdges[0];
	
	faceEdgesValues = [];
	
	for i in range(0,len(listFaceEdges),1):
		value = (componentName + "." + "e[%d]" %int(listFaceEdges[i]));
		print value;
		faceEdgesValues.append(value);
	
	return faceEdgesValues;

def returnEdgeVerts(edge):
	edgeNameSplit = edge.split(".");
	componentName = edgeNameSplit[0];
	
	mc.select(edge);
	edgePoints = mc.polyInfo(edgeToVertex=1);
	listEdgePoints = edgePoints[0].split("      ")
	listEdgePoints[(len(listEdgePoints)-1)] = listEdgePoints[(len(listEdgePoints)-1)].replace("  Hard\n","");
	
	del listEdgePoints[1];
	del listEdgePoints[0];
	
	edgePointValues = [];
	
	for i in range(0,len(listEdgePoints),1):
		edgePointValues.append(componentName + "." + "vtx[%d]" %int(listEdgePoints[i]));
	
	return edgePointValues;

def checkForIntersects(edgeVertList, faceVertList):
	
	print "empty";

def addPoint(vectorA,vectorB):
	returnVector = om.MVector();
	returnVector = vectorA + vectorB;
	return returnVector;

def subtractPoint(pointA, pointB):
	returnPoint = point();
	returnPoint.x = (pointA.x - pointB.x);
	returnPoint.y = (pointA.y - pointB.y);
	returnPoint.z = (pointA.z - pointB.z);
	return returnPoint;
	
def dotProduct(pointA, pointB):
	return [(pointA.x * pointB.x) + (pointA.y * pointB.y) + (pointA.z * pointB.z)];

def multiplyByFactor(pointA, factor):
	pointA.x *= factor;
	pointA.y *= factor;
	pointA.z *= factor;

def returnNormalOfFace(polyInfoString):
	returnPoint = point();
	str

#make sure it's the actual object node - ie, the [0] val
checkForIntersections("pCube1", "pCube2")
