###################################
###################################
#turtlegraphics_3D

import maya.cmds as mc;
import math as math;
from random import randint;
from random import uniform;

class drawer(object):
	x = 0;
	y = 0;
	z = 0;
	
	prevX = 0;
	prevY = 0;
	prevZ = 0;
	
	#XZ orientation
	orientationY = 0;
	prevOrientationY = 0;
	#XY orientation
	orientationZ = 0;
	prevOrientationZ = 0;
	radius = 0;

	def __init__(self):
		self.x = 0;
		self.y = 0;
		self.orientationY = 0;
		self.prevOrientationY = 0;
		self.orientationZ = 0;
		self.prevOrientationZ = 0;
		self.radius = 1;
		
	def resetO(self):
	    self.prevOrientationY = self.orientationY;
	    self.prevOrientationZ = self.orientationZ;
	    self.orientationY = 0;
	    self.orientationZ = 0;
	
	def resetOY(self):
	    self.prevOrientationY = self.orientationY;
	    self.orientationY = 0;
		
	def resetOZ(self):
	    self.prevOrientationZ = self.orientationZ;
	    self.orientationZ = 0;
	
	def resetP(self):
		self.setX(0);
		self.setY(0);
		self.setZ(0);
		
	def setX(self,num):
		self.prevX = self.x;
		self.x = num;
		
	def setY(self,num):
		self.prevY = self.y;
		self.y = num;
		
	def setZ(self,num):
		self.prevZ = self.z;
		self.z = num;
	
	def draw (self, distance):
		d = 1;
		
		#newX = (distance * (math.cos(self.orientation))) + self.x;
		#newY = self.y;
		#newZ = (distance * (math.sin(-self.orientation))) + self.z;
		
		self.setX(self.x + ((distance * math.cos(self.orientationZ)) * (math.sin(self.orientationY))));
		self.setY(self.y + (distance * math.sin(self.orientationZ)));
		self.setZ(self.z + ((distance * math.cos(self.orientationZ)) * (math.cos(self.orientationY))));
		
		drawCurve = mc.curve(d= 1, p=[(self.prevX,self.prevY,self.prevZ), (self.x,self.y,self.z)]);
		print "drawCurve:";
		print drawCurve;
		
		drawCylinder = self.createCylinder(self.prevX, self.prevY, self.prevZ, self.x, self.y, self.z);
		
		mc.select(drawCylinder[0]);
		
		mc.polyExtrudeFacet(inputCurve=drawCurve);
		mc.refresh();
		
	def turn(self, degreeY, degreeZ=0):
		print ">>>>>>>>>>>>>>>>>>>>>>>>"
		print "TURN"
		self.prevOrientationY = self.orientationY;
		self.prevOrientationZ = self.orientationZ;
		self.orientationY += math.radians(degreeY);
		self.orientationZ += math.radians(degreeZ);
			
		print "orientation:";
		print self.orientationY;
		print self.orientationZ;
		print ">>>>>>>>>>>>>>>>>>>>>>>>"
		
		
	#internal functions = do not call	
	def createCylinder(self, x, y, z, endX, endY, endZ):
		subdivs = 20;

		thisCylinder = mc.polyCylinder(radius=self.radius);

		mc.setAttr(thisCylinder[1]+".subdivisionsCaps", 1);

		mc.select(thisCylinder[0] + ".f" + "[" + str(0) + ":" + str((subdivs*2)-1) + "]");
		mc.delete();
		
		loc = mc.spaceLocator(p=(endX, endY, endZ));
		mc.xform(loc, cp=True);
		
		mc.xform(thisCylinder[0], cp=True);
		mc.select(thisCylinder[0]);
		mc.move(x,y,z, rpr=True);
		
		mc.aimConstraint(loc, thisCylinder[0], aimVector=(0,1,0));

		return thisCylinder;
		
	def returnReferenceAngle(self, angleAsDegrees):
	    returnAngle = 0;
	    if(angleAsDegrees >= 0 and angleAsDegrees <=90):
	        returnAngle = angleAsDegrees;
	    if(angleAsDegrees > 90 and angleAsDegrees <= 180):
	        returnAngle = 180 - angleAsDegrees;
	    if(angleAsDegrees > 180 and angleAsDegrees <= 270):
	        returnAngle = angleAsDegrees - 180;
	    if(angleAsDegrees > 270 and angleAsDegrees <= 360):
	        returnAngle = 360 - angleAsDegrees;
	    return returnAngle;

	def getVector(self, xOne,yOne,zOne, xTwo,yTwo,zTwo):
		xRes = xTwo-xOne;
		yRes = yTwo-yOne;
		zRes = zTwo-zOne;
		return xRes,yRes,zRes;
		

		
		
		
		
		
		
		
		
		
		
		
		
def drawOne():
	d = drawer();
	d.draw(5);
	
def drawFlatSquare():
	d = drawer();
	d.draw(5);
	d.turn(90,0);
	d.draw(5);
	d.turn(90,0);
	d.draw(5);
	d.turn(90,0);
	d.draw(5);
	
def drawVertSquare():
	d = drawer();
	d.draw(5);
	d.turn(0, 90);
	d.draw(5);
	d.turn(0, 90);
	d.draw(5);
	d.turn(0, 90);
	d.draw(5);
	
def drawAngledSquare():
	d = drawer();
	d.turn(45,45);
	d.draw(5);
	d.turn(0, 90);
	d.draw(5);
	d.turn(0, 90);
	d.draw(5);
	d.turn(0, 90);
	d.draw(5);
	
def drawStar():
	d=drawer();
	for j in range(0,12,1):
	    d.turn(15,15);
	    for i in range(0,10,1):
			d.draw(10);
			d.turn(0,90);
			d.draw(10);
			d.turn(0,90);
			d.draw(10);
			d.turn(0,90);
			d.draw(10);
			d.turn(0,10);
			
def drawRando():
	d=drawer();
	randDeg = randint(1,90);
	randNumStars = randint(1,50);
	randTurn = randint(1,15);
	randNumPoints = randint(1,50);
	randDrawLength = randint(1,10);
	randSTurnY = randint(0,5);
	randSTurnZ = randint(10,180);
	
	for i in range(0,randint(1,50),1):
		print "i"
		print "************************************************************************************************"
		d.resetO();
		d.turn(randDeg*i, randDeg*i);
		
		for j in range(0,randNumStars,1):
			print "j"
			print "************************************************************************************************"
			d.turn(0,randTurn);
			for k in range (0,randNumPoints,1):
				print "k"
				print "************************************************************************************************"
				d.draw(randDrawLength);
				d.turn(randSTurnY,randSTurnZ);

				
def drawRandoTwo():
	d=drawer();
	#d.radius = 1;
	d.radius = uniform(0,1);
	randDeg = randint(1,90);
	randDegCurrent = randDeg;
	randNumStars = randint(1,30);
	randTurn = randint(1,15);
	randNumPoints = randint(1,10);
	#at 5,15 and radius 5, tends to produce "rose" structures
	randDrawLength = randint(15,35);
	randSTurnY = randint(0,5);
	randSTurnZ = randint(10,180);
	randSTurnYTwo = randint(0,5);
	randSTurnZTwo = randint(10,180);
	
	for i in range(0,randint(1,10),1):
		print "i"
		print "************************************************************************************************"
		d.resetO();
		d.resetP();
		d.turn(randDegCurrent, randDegCurrent);
		randDegCurrent += randDeg;
		
		for j in range(0,randNumStars,1):
			print "j"
			print "************************************************************************************************"
			d.turn(0,randTurn);
			for k in range (0,randNumPoints,1):
				print "k"
				print "************************************************************************************************"
				d.draw(randDrawLength);
				d.turn(randSTurnY,randSTurnZ);
				d.draw(randDrawLength);
				d.turn(randSTurnYTwo,randSTurnZTwo);
			

def drawRandoMulti():
	d=drawer();
	e=drawer();
	#d.radius = 1;
	d.radius = uniform(0,1);
	e.radius = uniform(0,1);
	
	randDeg = randint(1,90);
	randDegTwo = randint(1,90);
	
	randDegCurrent = randDeg;
	randDegCurrentTwo = randDegTwo;
	
	randNumStars = randint(1,30);
	
	randTurn = randint(1,15);
	randTurnTwo = randint(1,15);
	
	randNumPoints = randint(1,10);
	#at 5,15 and radius 5, tends to produce "rose" structures
	randDrawLength = randint(15,35);
	randDrawLengthTwo = randint(15,35);
	
	randSTurnY = randint(0,5);
	randSTurnZ = randint(10,180);
	randSTurnYTwo = randint(0,5);
	randSTurnZTwo = randint(10,180);
	
	randSTurnYTwo = randint(0,5);
	randSTurnZTwo = randint(10,180);
	randSTurnYTwoTwo = randint(0,5);
	randSTurnZTwoTwo = randint(10,180);
	
	for i in range(0,randint(1,10),1):
		print "i"
		print "************************************************************************************************"
		d.resetO();
		e.resetO();
		d.resetP();
		e.resetP();
		d.turn(randDegCurrent, randDegCurrent);
		e.turn(randDegCurrentTwo, randDegCurrentTwo);
		randDegCurrent += randDeg;
		
		for j in range(0,randNumStars,1):
			print "j"
			print "************************************************************************************************"
			d.turn(0,randTurn);
			e.turn(0,randTurnTwo);
			for k in range (0,randNumPoints,1):
				print "k"
				print "************************************************************************************************"
				d.draw(randDrawLength);
				e.draw(randDrawLengthTwo);
				d.turn(randSTurnY,randSTurnZ);
				e.turn(randSTurnYTwo,randSTurnZTwo);
				d.draw(randDrawLength);
				e.draw(randDrawLength);
				d.turn(randSTurnYTwo,randSTurnZTwo);			
				e.turn(randSTurnYTwoTwo,randSTurnZTwoTwo);


def drawRandoMultiTwo():
	d=drawer();
	e=drawer();
	#d.radius = 1;
	d.radius = uniform(0,1);
	e.radius = uniform(0,1);
	
	randDeg = randint(1,35);
	randDegTwo = randint(1,180);
	
	randDegCurrent = randDeg;
	randDegCurrentTwo = randDegTwo;
	
	randNumStars = randint(1,50);
	
	randTurn = randint(1,15);
	randTurnTwo = randint(1,40);
	
	randNumPoints = randint(1,25);
	#at 5,15 and radius 5, tends to produce "rose" structures
	randDrawLength = randint(15,35);
	randDrawLengthTwo = randint(10,40);
	
	randSTurnY = randint(0,5);
	randSTurnZ = randint(10,180);
	randSTurnYTwo = randint(0,25);
	randSTurnZTwo = randint(10,180);
	
	randSTurnYTwo = randint(0,5);
	randSTurnZTwo = randint(10,180);
	randSTurnYTwoTwo = randint(0,25);
	randSTurnZTwoTwo = randint(10,180);
	
	for i in range(0,randint(1,10),1):
		print "i"
		print "************************************************************************************************"
		d.resetO();
		e.resetO();
		d.resetP();
		e.resetP();
		d.turn(randDegCurrent, randDegCurrent);
		e.turn(randDegCurrentTwo, randDegCurrentTwo);
		randDegCurrent += randDeg;
		
		for j in range(0,randNumStars,1):
			print "j"
			print "************************************************************************************************"
			d.turn(0,randTurn);
			e.turn(0,randTurnTwo);
			for k in range (0,randNumPoints,1):
				print "k"
				print "************************************************************************************************"
				d.draw(randDrawLength);
				e.draw(randDrawLengthTwo);
				d.turn(randSTurnY,randSTurnZ);
				e.turn(randSTurnYTwo,randSTurnZTwo);
				d.draw(randDrawLength);
				e.draw(randDrawLength);
				d.turn(randSTurnYTwo,randSTurnZTwo);			
				e.turn(randSTurnYTwoTwo,randSTurnZTwoTwo);

				

#drawOne();
#drawFlatSquare();
#drawVertSquare();
#drawAngledSquare();
#drawStar();
#drawRando();
#drawRandoTwo();
#drawRandoMulti();
drawRandoMultiTwo();