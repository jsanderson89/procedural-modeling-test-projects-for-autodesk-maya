# finding the location of a component:
# form = mc.xform('pSphere2.vtx[249]', query = 1, worldSpace=1, translation=1)
# print form;

#pointPosition  polySurface1.vtx[11]

#update viewport = mc.refresh();

###################################
###################################
#genBubbles

import maya.cmds as mc;
from random import randint;
from random import choice;
from random import uniform;

def createBubbles(maxNumberOfBubbles=50):
	numberOfSpheres = randint(0,maxNumberOfBubbles);
	listOfObjects = createBase();
	
	for i in range(0, numberOfSpheres+1, 1):
		print "#########################################"
		print i;
		randomObject = choice(listOfObjects);
		# print randomObject[0];
		
		listOfFaces = getObjectFaces(randomObject);
		baseFace = choice(listOfFaces);
		
		makeSphere = createSphere(baseFace, randomObject[0]);
		# print makeSphere;
		
		listOfObjects.append(makeSphere);
		print "sphere {} of {} created.".format(i,numberOfSpheres);
		mc.refresh();
		print "#########################################"
	

def createBase():
	returnList = [];
	baseSphere = mc.polySphere(r=2, sx=20,sy=20,ax=(0,1,0))
	returnList.append(baseSphere);
	return returnList;

def selRandomObject(listOfObjects):
	selection = choice(listOfObjects);
	return selection;
	
def getObjectFaces(obj):
	returnListOfFaces = [];
	numberOfFaces = mc.polyEvaluate(obj[0], face=1);
	
	for i in range(0,numberOfFaces,1):
		returnListOfFaces.append(obj[0] + ".f" + "[" + str(i) + "]");
	
	print "return LOF"
	print returnListOfFaces;
	return returnListOfFaces;
	
def createSphere(baseFace, object):
	randomRadius = uniform(0,2);
	print randomRadius;
	
	faceVertsQuery = mc.polyInfo(baseFace, faceToVertex=1);
	faceVerts = getVertsFromQuery(faceVertsQuery, object);
	print faceVerts;
	
	vertCoords = getVertCoords(faceVerts);
	print vertCoords;
	
	centerPoint = getFaceCenter(vertCoords);
	print "cp"
	print centerPoint[0];
	
	createdSphere = mc.polySphere(r=randomRadius);
	mc.xform(createdSphere[0], translation = (centerPoint[0],centerPoint[1],centerPoint[2]), a=1);
	
	return createdSphere;

def getVertsFromQuery(query, obj):
	infoList = query[0].split("    ");
	print infoList;
	infoList[len(infoList)-1] = infoList[len(infoList)-1].replace(" \n", "");
	
	del infoList[1];
	del infoList[0];
	
	for i in range(0,len(infoList), 1):
		infoList[i].strip();
		infoList[i] = (obj + ".vtx" + "[" + infoList[i] + "]");
	
	return infoList;

def getVertCoords(vertList):
	outputList = [];
	for i in range(0,len(vertList),1):
		outputList.append(mc.xform(vertList[i], query = 1, worldSpace=1, translation=1));
		print outputList[i];
	
	return outputList;
	
def getFaceCenter(polygonVertList):
	px=0;
	py=0;
	pz=0;
	
	for i in range (0,len(polygonVertList),1):
		px += polygonVertList[i][0];
		py += polygonVertList[i][1];
		pz += polygonVertList[i][2];
	
	cp = [px/len(polygonVertList), py/len(polygonVertList),pz/len(polygonVertList)];
	
	print "creating locator....";
	mc.spaceLocator(p=(cp[0], cp[1],cp[2]));
	
	return cp;

createBubbles(200);
