# import maya.cmds as cmds

# cmds.polyPlane( sx=3, sy=3 )

# add a new face
# cmds.polyAppendVertex( a=[6, 10, (.167, .3, -.167), (.167, .3, .167)] )

# add a new holed face
# cmds.polyAppendVertex( a=[9, 5, (-.167, .3, .167), (-.167, .3, -.167), (), (-.167, .2, .1), (-.167, .1, 0), (-.167, .2, -.1)] )



###################################
###################################
#proceduralTest

import maya.cmds as mc;
import math as math;
from random import randint;
from random import uniform;


def Main():
	#polyPlaneCaller()
	drawPlane(1)
	
def polyPlaneCaller():
	mc.polyPlane(sx=1,sy=1);
	
def drawPlane(scaler=5):
    mc.polyCreateFacet(p=[(0,0,0), (scaler,0,0), (0,0,-scaler)])
    mc.polyAppendVertex(a=[1,2, (scaler,0,-scaler)])
	
Main()