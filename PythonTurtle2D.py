###################################
###################################
#turtlegraphics_2D

import maya.cmds as mc;
import math as math;

class drawer(object):
	x = 0;
	y = 0;
	orientation = 0;
	radius = 0;

	def __init__(self):
		self.x = 0;
		self.y = 0;
		self.orientation = 0;
		self.radius = 5;
		

	def mark(self, distance):
		#d=1 is straight line; 3=curve
		#num of points  = d+1
		d = 1;

		# x=distance cos angle, y=distance sin angle
		newX = (distance * (math.cos(self.orientation))) + self.x;
		newY = (distance * (math.sin(self.orientation))) + self.y;
		
		mc.curve(d= 1, p=[(self.x,self.y,0), (newX,newY,0)]);
		
		self.x = newX;
		self.y = newY;
	
	def draw (self, distance):
		d = 1;
		oldX = self.x;
		oldY = self.y;
		newX = (distance * (math.cos(self.orientation))) + self.x;
		newY = (distance * (math.sin(self.orientation))) + self.y;
		
		drawCurve = mc.curve(d= 1, p=[(self.x,self.y,0), (newX,newY,0)]);
		print "drawCurve:";
		print drawCurve;
		
		self.x = newX;
		self.y = newY;
		print math.degrees(self.orientation)
		drawCylinder = self.createCylinder(oldX, oldY, math.degrees(self.orientation)+90);
		
		mc.select(drawCylinder[0]);
		
		mc.polyExtrudeFacet(inputCurve=drawCurve);
		mc.refresh();
		
	def turn(self, degree):
		self.orientation += math.radians(degree);
		print "orientation:";
		print self.orientation;
		
		
	#internal functions = do not call	
	def createCylinder(self, x, y, turtleOrientation=90):
		subdivs = 20;
		
		thisCylinder = mc.polyCylinder(radius=.3);
		mc.rotate(0,0,turtleOrientation, thisCylinder[0]);
		mc.setAttr(thisCylinder[1]+".subdivisionsCaps", 1);
		
		
		mc.select(thisCylinder[0] + ".f" + "[" + str(0) + ":" + str((subdivs*2)-1) + "]");
		mc.delete();
		
		mc.xform(thisCylinder[0], cp=True);
		#mc.xform(thisCylinder[0], translation = (x,0,0) , absolute=True, ws=True);
		mc.select(thisCylinder[0]);
		mc.move(x,y,0, rpr=True);
		
		return thisCylinder;



def drawOne():
	d = drawer();
	d.draw(5);
	
def drawSquare():
	d = drawer();
	d.draw(5);
	d.turn(90);
	d.draw(5);
	d.turn(90);
	d.draw(5);
	d.turn(90);
	d.draw(5);
	
def drawStar():
	d=drawer();
	for i in range(0,10,1):
		d.draw(5);
		d.turn(90);
		d.draw(5);
		d.turn(90);
		d.draw(5);
		d.turn(90);
		d.draw(5);
		
		d.turn(10);

drawStar();